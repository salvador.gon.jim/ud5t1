﻿namespace UD5T1
{
    public partial class MainPage : ContentPage
    {
        decimal cuenta;
        int propina;
        int personas = 1;
        public MainPage()
        {
            InitializeComponent();
        }
        //Metodo que calcula y cambia los valores de lblPropinaPorPersona, lblSubtotal, lblTotal
        private void CalcularTotal()
        {
            // Propina total
            var propinaTotal = cuenta * propina / 100;
            // Propina por persona
            var propinaPorPersona = propinaTotal / personas; 
            lblPropinaPorPersona.Text = $"{propinaPorPersona:C}";
            // Subtotal
            var subtotal = cuenta / personas; 
            lblSubtotal.Text = $"{subtotal:C}";
            // Total
            var totalPorPersona = (cuenta + propinaTotal) / personas; 
            lblTotal.Text = $"{totalPorPersona:C}";
        }
        //Metodo que extrae el valor de txtCuenta y lo guarda en la variable cuenta
        private void TxtCuenta_Completed(object sender, EventArgs e)
        {
            cuenta = Convert.ToDecimal(txtCuenta.Text);
            CalcularTotal();
        }
        //Metodo que extrae el valor de sldPropina y lo guarda en la variable propina
        private void SldPropina_ValueChanged(object sender, ValueChangedEventArgs e)
        {
            propina = Convert.ToInt32(sldPropina.Value);
            lblPropina.Text = $"Propina: {propina}%";
            CalcularTotal();
        }
        //Metodo que envia el valor de los botones a el slider sldPropina
        private void Button_Clicked(object sender, EventArgs e)
        {
            if (sender is Button)
            {
                var btn = (Button)sender;
                var porcentaje = int.Parse(btn.Text.Replace("%", string.Empty));
                sldPropina.Value = porcentaje;
            }
        }
        //Metodo que comprueba que haya mas de una persona para decrementar su valor
        private void BtnMenos_Clicked(object sender, EventArgs e)
        {
            if (personas > 1)
            {
                personas--;
                lblPersonas.Text = $"{personas}";
                CalcularTotal();
            }
        }
        //Metodo que incrementa el valor de la variable personas
        private void BtnMas_Clicked(object sender, EventArgs e)
        {
            personas++;
            lblPersonas.Text = $"{personas}";
            CalcularTotal();
        }
    }
}
